import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { Model } from './base.model';

// https://github.com/wgenial/cartrolandofc/blob/master/nova-api.md
// extensão do chrome para resolver problema com o CORS
// https://chrome.google.com/webstore/detail/cors-toggle/jioikioepegflmdnbocfhgmpmopmjkim
// desabilitando segunrança do chrome, executar no terminal: google-chrome --disable-web-security
export class BaseService<T extends Model> {

  endPoint: string;

  constructor(protected http: HttpClient, domain: string) {
    this.endPoint = 'https://api.cartolafc.globo.com/' + domain;
  }
	
  get(subroute: string, slug: string = ''): Observable<T> {
	const route = this.endPoint;
	if (subroute && subroute !== '') { route += `${subroute}/`; };
	if (slug && slug !== '') { route += slug };
    return this.http.get<T>(route);
  }

  save(data: T): Observable<T> {
    let stream: Observable<T>;

    // tslint:disable-next-line:no-null-keyword
    if (data.id === undefined || data.id === null) {
      stream = this.http.post<T>(`${this.endPoint}`, data);
    } else {
      stream = this.http.put<T>(`${this.endPoint}/${data.id}`, data);
    }

    return stream;
  }

  query(filters: any = null): Observable<T> {
    return this.http.get<T>(this.endPoint, { params: this.mapFilters(filters) });
  }

  post(subroute: string, body: any, reqOpts?: any):Observable<ArrayBuffer> {
    if (subroute && subroute !== '') { subroute = `/${subroute}`; };
    return this.http.post(this.endPoint + subroute, body, reqOpts);
  }

  private mapFilters(filters: any[]): HttpParams {
    let params = new HttpParams();

    for (let filter in filters) {
      params = params.append(filter, filters[filter]);
    }

    return params;
  }

}
