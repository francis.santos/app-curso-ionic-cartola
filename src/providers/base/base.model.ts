export abstract class Model {
  id?: any;
  
  constructor(obj: Model = {} as Model) {
    Object.assign(this, obj);
  }
}
