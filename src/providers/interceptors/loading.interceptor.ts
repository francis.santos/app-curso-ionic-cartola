import { AccountService } from './../account/account.service';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/do';
import { LoadingController } from 'ionic-angular';
import { HttpErrorResponse } from '@angular/common/http/src/response';

import { LoadingService } from './loading.service';
import { AlertController, App } from 'ionic-angular';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  constructor(private loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public app: App,
              private _accountService: AccountService) {}

  showAlert(msg:string, callback: any = null) {
    let alert = this.alertCtrl.create({
      title: 'Atenção',
      subTitle: msg,
      buttons: [{
        text: 'Ok',
        role: 'cancel',
        handler: () => {
          if (callback) callback();
        }
      }],
      enableBackdropDismiss: false,
    });
    alert.present();
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Aguarde...'
    });

    if (LoadingService.showLoading) {
      loading.present();
    }

    let dupReq = req;
    //const account = this._accountService.getAccount();
    const account = JSON.parse(localStorage.getItem(this._accountService.userAuth));
    if (account) {
      dupReq = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${account.token}`)
      });
    }

    return next.handle(dupReq).do(
      (event: any) => {
        console.log(req);
      },
      (err: HttpErrorResponse) => {
        if (LoadingService.showToast) {
          let msg = 'Ocorreu um erro, contate o administrador do sistema.';
          let callback = null;
          let nav = this.app.getActiveNav();
          console.log(err);
          if(err.error.error) {
            msg = err.error.error;
            if (msg === 'token_not_provided') {
              msg = 'Você não tem permissão para acessar esta página.';
              callback = () => {
                nav.pop();
              }
            } else if (msg === 'messages.login.invalidCredentials') {
              msg = 'Credenciais Inválidas.';
            } else if (msg === 'token_expired') {
              msg = 'Você foi deslogado do sistema por inatividade. Favor entrar no sistema novamente.';
              callback = () => {
                this._accountService.logoutAccount();
                nav.popToRoot();
              }
            }
          } else {
            msg = '';
            for (let item in err.error) {
              msg += err.error[item] + '<br>';
            }
          }
          this.showAlert(msg, callback);
        }
      }).
      finally(() => {
        if (loading) {
          loading.dismiss();
        }
      });
  }

}
