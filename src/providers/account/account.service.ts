import { Account } from './account.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { Observable } from 'rxjs/Observable';
import { forEach as _forEach, findIndex as _findIndex } from 'lodash';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/map';

const USER_AUTH = 'user_auth';

@Injectable()
export class AccountService extends BaseService<Account> {

  userAuth = USER_AUTH;

  isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loggedUser: BehaviorSubject<Account> = new BehaviorSubject<Account>(undefined);

  constructor(private _http: HttpClient) {
    super(_http, 'authenticate');
  }

  public authenticate(account: Account): Observable<Account>{
    return this.post('', account)
      .map((account: any) => {
        this.updateSession(account);
        return account;
      });
  }

  private updateSession(account: Account) {
    if (account !== undefined) {
      localStorage.setItem(USER_AUTH, JSON.stringify(account));
      this.isLoggedIn.next(true);
    } else {
      localStorage.removeItem(USER_AUTH);
    }

    this.loggedUser.next(account);
  }

  public logoutAccount(){
    this.updateSession(undefined);
  }

  public getAccount():Account{
    return this.loggedUser ? this.loggedUser.getValue() : null;
  }

  public getUser():any{
    const account = this.loggedUser ? this.loggedUser.getValue() : null;
    return (account) ? account.user : null;
  }

  public getUserId(): number{
    const account = this.loggedUser ? this.loggedUser.getValue() : null;
    return (account) ? account.user.id : 0;
  }

}
