import { User } from './user.model';
import { BaseService } from './../base/base.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class UserService extends BaseService<User> {

  constructor(public http: HttpClient) {
    super(http, 'users');
  }

}
